NODE_BIN=		'node_modules/.bin'
NODE_SASS=		./${NODE_BIN}/node-sass
SHELL=			/bin/bash
TEST_DIFF_CSS=	diff -q "tests/$(1).css" <( ${NODE_SASS} "tests/$(1).sass" )

.PHONY: usage
usage:
	@echo Usage: make [task]

.PHONY: test
test: test-main test-2x-scale

.PHONY: test-main
test-main:
	$(call TEST_DIFF_CSS,main)

.PHONY: test-2x-scale
test-2x-scale:
	$(call TEST_DIFF_CSS,2x-scale)
