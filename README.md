# Sass Mix Ratio library

Sass/SCSS functions for mixing color amounts specified by ratios.

## Installation

```shell
npm install --save sass-mix-ratio
```

## Usage

```sass
@import '~sass-mix-ratio'

.your-class

    // Mix 1 part of red with 2 parts of blue:
    color:              mix-ratio( red, blue, 1, 2 )
    // Mix 1 part of yellow with 2 parts of white:
    background-color:   tint-ratio( yellow, 2 )
    // Mix 3 parts of green with 5 parts of black:
    border-color:       shade-ratio( green, 5, 3 )
```

### Functions for 2x scale tints and shades 

```sass
@import '~sass-mix-ratio/2x-scale'

.your-class

    // Mix 1 part of yellow with 2 parts of white:
    background-color:   tint-2( yellow )
    // Mix 4 parts of blue with 1 part of black:
    border-color:       shade-1-4( blue )
```

## API

### Main Functions

```sass
@import '~sass-mix-ratio'
```

####    mix-ratio( $color1, $color2, $ratio1: 1, $ratio2: 1 )

####    tint-ratio( $color, $white-ratio: 1, $color-ratio: 1 )

####    shade-ratio( $color, $black-ratio: 1, $color-ratio: 1 )

### 2x Scale for Tints and Shades

```sass
@import '~sass-mix-ratio/2x-scale'
```

####    tint-1( $color )
####    tint-2( $color )
####    tint-4( $color )
####    tint-8( $¢olor )
####    tint-16( $color )
####    tint-32( $color )
####    tint-64( $color )
####    tint-1-2( $color )
####    tint-1-4( $color )
####    tint-1-8( $color )
####    tint-1-16( $color )
####    tint-1-32( $color )
####    tint-1-64( $color )

####    shade-1( $color )
####    shade-2( $color )
####    shade-4( $color )
####    shade-8( $¢olor )
####    shade-16( $color )
####    shade-32( $color )
####    shade-64( $color )
####    shade-1-2( $color )
####    shade-1-4( $color )
####    shade-1-8( $color )
####    shade-1-16( $color )
####    shade-1-32( $color )
####    shade-1-64( $color )


##  License

Sass-mix-ratio is [Apache-2.0 licensed](https://gitlab.com/tokenmill/npm/sass-mix-ratio/blob/master/LICENSE).
